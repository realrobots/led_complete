# This file contains all the animations and helper functions for the LED matrix
# They should be called from code.py

import random
import time

pixels = None

# Sets the 'pixels' variable, so that the helper functions can access the NeoPixel object
def set_pixels(_pixels):
    global pixels
    pixels = _pixels

# Fills the screen with a single colour
def fill_screen( color = (0,0,0), show=True):
    for i in range(len(pixels)):
        pixels[i] = color

    if show:
        pixels.show()

# Fills the screen with a single colour, but does so slowly
def fill_screen_slow( color = (0,0,0)):
    for i in range(len(pixels)):
        pixels[i] = color

        pixels.show()

# Sets a single pixel to a specific colour
def set_pixel( x, y, color=(0,0,0), show=False):
    if x < 0 or x > 15 or y < 0 or y > 15:
        return
    if y % 2 == 0:
        x = 15-x
    pixels[x + y * 16] = color

    if show:
        pixels.show()


# Sets random colours to random pixels
def disco():
    for i in range(500):
        pixels[random.randint(0, 255)] = random_colour()
        pixels.show()
        time.sleep(0.01)

# Fades the screen to a specific colour, one pixel at a time
def fade_to_colour(color=(0,0,0)):
    pixel_order = []
    for i in range(256):
        pixel_order.append(i)
    pixel_order = shuffle_list(pixel_order)
    for i in pixel_order:
        pixels[i] = color 
        pixels.show()
        time.sleep(0.01)


# Cycles the entire screen through the colour spectrum
def cycle_colours():        
    print("Start Program: Cycle Colours")
    increment = 6

    for i in range(0, 256, increment):
        fill_screen((i,0,0), True)

    for i in range(2):
        for i in range(0, 256, increment):
            fill_screen((255,i,0), True)
        for i in range(0, 256, increment):
            fill_screen((255-i,255,0), True)
        for i in range(0, 256, increment):
            fill_screen((0,255,i), True)
        for i in range(0, 256, increment):
            fill_screen((0,255-i,255), True)
        for i in range(0, 256, increment):
            fill_screen((i,0,255), True)
        for i in range(0, 256, increment):
            fill_screen((255,0,255-i), True)


def rain():
    print("Start Program: Rain")
    frames_per_second = 50
    color = (0,0,255)

    cols = []
    for i in range(16):
        cols.append(-1)

    for i in range(0, 350):
        startTime = time.monotonic()

        fill_screen((0,0,0))

        for x in range(len(cols)):                
            
            if cols[x] > 15 and cols[x] < 30:
                set_pixel(x, 0, color)
                pass
            else:
                set_pixel(x, 15-cols[x], color)
                pass

        pixels.show() 

        

        for x in range(len(cols)):
            if cols[x] > -1:
                cols[x] = cols[x] + 1
                if cols[x] == 30:
                    cols[x] = -1

        if random.randrange(0, 4) == 0:
            r = random.randrange(0, 16)
            if cols[r] == -1:
                cols[r] = 0

        pixels.show()

        while time.monotonic() - startTime < 1/frames_per_second:
            time.sleep(0.001)

# Animates Mario running
def MarioRun():
        d = 0.1
        LoadImage("mario.bmp")
        time.sleep(d)
        LoadImage("mario1.bmp")
        time.sleep(d)
        for i in range(10):
            LoadImage("mario2.bmp")
            time.sleep(d)
            LoadImage("mario3.bmp")
            time.sleep(d)
            LoadImage("mario2.bmp")
            time.sleep(d)
            LoadImage("mario1.bmp")
            time.sleep(d)

# Loads an image from a file and displays it on the screen
# File should be in CIRCUITPY/images/
def LoadImage(filename):

        filename = "images/" + filename
        data = []
        header = []
        print("Drawing: " + filename)            

        # Load Header and data from file
        with open(filename, "rb") as f:
            byte = f.read(1)
            count = 0
            b = 0
            while byte != b"":
                byte = f.read(1)
                count+=1

                if count < 40:
                    header.append(byte)
                else:
                    data.append(byte)
            
        c = 0
        start = (len(data) - 256*3) - 1
        row = 0
        offset = 15
        pixrows = []
        pixrows.append([])
        for i in range(start, start + 256*3, 3):
            r = int.from_bytes(data[i+2], "little")
            g = int.from_bytes(data[i+1], "little")
            b = int.from_bytes(data[i+0], "little")
            color = (r, g, b)
            if c % 16 == 0:
                row += 1
                pixrows.append([])
            
            pixrows[row].append(color)
            c += 1
        for i in range(0, len(pixrows)-1):
            for p in range(16):
                if i % 2 == 0:
                    pixels[p + i*16] = pixrows[i+1][p]
                else:
                    pixels[15-p + i*16] = pixrows[i+1][p]
        pixels.show()


## HELPER FUNCTIONS ##

# Returns a random colour
def random_colour():
    r = random.randint(0, 255)
    g = random.randint(0, 255)
    b = random.randint(0, 255)
    return (r, g, b)

# Shuffles a list
def shuffle_list(lst):
    result = []
    while lst:
        i = random.randint(0, len(lst)-1)
        result.append(lst.pop(i))
    return result