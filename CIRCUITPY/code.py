import time
import board
import neopixel
import helper

pixel_pin = board.GP29 # This may be different for you check before copying over this
num_pixels = 256

pixels = neopixel.NeoPixel(pixel_pin, num_pixels, brightness=0.05, auto_write=False)

# Sets the first pixel to red
pixels[0] = (255, 0, 0)
# Shows this change on the display
pixels.show()

# Passes the pixels to the helper functions NEED TO LEAVE THIS HERE
helper.set_pixels(pixels)

while True: 
    helper.LoadImage("cptamerica.bmp")
    time.sleep(1)
    helper.rain()
    helper.cycle_colours()
    helper.MarioRun()
    helper.LoadImage("hulk.bmp")
    time.sleep(1)
    helper.fade_to_colour()
    helper.LoadImage("ironman.bmp")
    time.sleep(1)
    helper.fade_to_colour((255,0,0))
    time.sleep(1)
    helper.disco()       
    helper.fill_screen_slow((3, 255, 0))
    time.sleep(1)


